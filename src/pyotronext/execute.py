"""
Copyright 2018  - 2020 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/execute.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
Contains (with modifications) parts of https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/execute.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
Contains (with modifications) parts of https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/execute_v3.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/execute.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018-2020 The Pyotronext authors 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018-2020 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/execute.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
Contains (with modifications) parts of https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/execute.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
Contains (with modifications) parts of https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/execute_v3.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot running Marlin firmware.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
This file contains the function get_protocol_api().
Status: Alpha
Language: Python 3.7
Credits: 
DerAndere: Founder, initial design and implementation, 
           core developer, current maintainer
Other pyotronext authors, see pyotronext AUTHORS file 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS): See 
CHANGELOG file (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/CHANGELOG)
Jack Minardi: Development of mecode
Opentrons Labworks Inc.: parts of execute.py, entrypoint_utils.py, 
                         protocols/parse.py, protocols/types.py; labware/*, 
                         design of the opentrons APIv2 (https://github.com/Opentrons/opentrons)

pyotronext is based on third-party software. Each third-party software is 
licensed under the terms of its separate license. The License text
can be found in the directory 3rd_party_licenses/ . The information in this
comment block pertains to the respective third-party software.

mecode (https://github.com/jminardi/mecode)
Copyright (c) 2014 Jack Minardi
License: MIT
Contributions: mecode was developed by the Lewis Lab at Harvard University. 

opentrons (https://github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0
SPDX-License-Identifier: Apache 2.0


@version 1.0.7
"""


import sys 
import argparse
import pathlib
from typing import NamedTuple
import pyotronext.configurator
import pyotronext.entrypoint_utils
import pyotronext.protocols.parse
import pyotronext.protocols.types
import pyotronext.protocol_api

def get_protocol_api(robotID=1,
                     direct_write=False,
                     direct_write_mode="serial",
                     outfile=None,
                     port=pyotronext.configurator.Config.config["port"],
                     baudrate=pyotronext.configurator.Config.config["baudrate"],
                     version=None
                     ):
    """
    This is the entrypoint for the interactive Python shell or for executing by 
    invocing the Python interpreter. It provides the user facing API. Example:
    ```(.py)
    import pyotronext.execute
    from pyotronext import protocol_api 
    
    def run(protocol: protocol_api.ProtocolContext):

    tiprack1 = protocol.load_labware(load_name="gep_96_tiprack_10ul, slot=1")
    plate1 = protocol.load_labware(load_name="corning_96_wellplate_360ul_flat", slot=1)

    pipette = protocol.load_instrument('p300_single', mount='left', tip_racks = [tiprack1])
    
    pipette.move_to(plate1["A1"])
    pipette.move_to(plate1["A2"].top(z=5)) # move 5mm above top of plate1 well "A2"

    pipette.teardown() # finish moves and disconnect
    
    protocol = pyotronext.execute.get_protocol_api(version="2.0") 
                                               
    run(protocol)
    ```
    """

    if isinstance(version, str):
        checked_version = pyotronext.protocols.parse.version_from_string(version)
    elif not isinstance(version, pyotronext.protocols.types.APIVersion):
        raise TypeError('version must be either a string or an APIVersion')
    else:
        checked_version = version
    if checked_version < pyotronext.protocols.types.APIVersion(2, 0):
        raise ValueError('version must be "2.0", APIVersion(2.0) or greater')

    context = pyotronext.protocol_api.ProtocolContext(
                                           robotID=robotID,
                                           direct_write=direct_write, 
                                           direct_write_mode=direct_write_mode, 
                                           outfile=outfile, 
                                           port=port, 
                                           baudrate=baudrate,
                                           api_version=checked_version)
    return context

def execute(protocol_file=None,
            protocol_name="",
            propagate_logs=False,
            log_level='warning',
            emit_runlog=None,
            custom_labware_paths=None,
            custom_data_paths=None,
            direct_write=False, 
            direct_write_mode="serial", 
            outfile=None, 
            port=pyotronext.configurator.Config.config["port"],
            baudrate=pyotronext.configurator.Config.config["baudrate"]):
    """
    entrypoint to run the protocol from the python shell:
    ```(py)
    protocol = pyotronext.execute.execute(protocol_file=None,
                                          protocol_name=None,
                                          custom_labware_paths=None,
                                          custom_data_paths=None,
                                          )  # entrypoint for the python shell to run the protocol 

    ```
    """
    contents = protocol_file.read()
    if custom_labware_paths:
        extra_labware = pyotronext.entrypoint_utils.labware_from_paths(custom_labware_paths)
    else:
        extra_labware = {}
    if custom_data_paths:
        extra_data = pyotronext.entrypoint_utils.datafiles_from_paths(custom_data_paths)
    else:
        extra_data = {}
    protocol = pyotronext.protocols.parse.parse(contents, protocol_name,
                     extra_labware=extra_labware,
                     extra_data=extra_data)
    
    context = get_protocol_api(version=getattr(protocol, 'api_level', pyotronext.protocols.types.APIVersion(2, 0)), 
                               direct_write=direct_write, 
                               direct_write_mode=direct_write_mode, 
                               outfile=outfile,
                               port=port,
                               baudrate=baudrate)
    run_protocol(protocol, context)

def run_protocol(protocol: pyotronext.protocols.types.PythonProtocol, context):
    if isinstance(protocol, pyotronext.protocols.types.PythonProtocol):
        if protocol.api_level >= pyotronext.protocols.types.APIVersion(2, 0):
            new_globs = {}
            exec(protocol.contents, new_globs)
            new_globs["__context"] = context
            try:
                exec("run(__context)", new_globs)
            except Exception as e:
                raise e
        else:
            raise RuntimeError("Unsupported python API version")

def get_arguments(
        parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    """ Get the argument parser for this module
    Useful if you want to use this module as a component of another CLI program
    and want to add its arguments.
    :param parser: A parser to add arguments to.
    :returns argparse.ArgumentParser: The parser with arguments added.
    """
    parser.add_argument(
        '-b', '--baudrate',
        action='store_const', 
        type=int, 
        default=pyotronext.configurator.Config.config["baudrate"],
        help='Specify baud rate, e.g. 115200')
    parser.add_argument(
        '-w', '--direct_write',
        action='store_true',
        help='If this flag is set, send G-code using serial communication via USB to execute the protocol on the robot')
    parser.add_argument(
        '-m', '--direct_write_mode',
        choices=['serial'],
        default='serial',
        help='Either \'serial\' or \'socket\'. Only used with --direct_write (-w).')
    parser.add_argument(
        '-l', '--log-level',
        choices=['debug', 'info', 'warning', 'error', 'none'],
        default='warning',
        help='Specify the level filter for logs to show on the command line. '
        'The logs stored in journald or local log files are unaffected by '
        'this option and should be configured in the config file. If '
        '\'none\', do not show logs')
    parser.add_argument(
        '-L', '--custom-labware-path',
        action='append', default=[pathlib.Path.cwd()],
        help='Specify directories to search for custom labware definitions. '
             'You can specify this argument multiple times. Once you specify '
             'a directory in this way, labware definitions in that directory '
             'will become available in ProtocolContext.load_labware(). '
             'Only directories specified directly by '
             'this argument are searched, not their children. JSON files that '
             'do not define labware will be ignored with a message. '
             'By default, the current directory (the one in which you are '
             'invoking this program) will be searched for labware.')
    parser.add_argument(
        '-D', '--custom-data-path',
        action='append', nargs='?', const='.', default=[],
        help='Specify directories to search for custom data files. '
             'You can specify this argument multiple times. Once you specify '
             'a directory in this way, files located in the specified '
             'directory will be available in ProtocolContext.bundled_data. '
             'Note that bundle execution will still only allow data files in '
             'the bundle. If you specify this without a path, it will '
             'add the current path implicitly. If you do not specify this '
             'argument at all, no data files will be added. Any file in the '
             'specified paths will be loaded into memory and included in the '
             'bundle if --bundle is passed, so be careful that any directory '
             'you specify has only the files you want. It is usually a '
             'better idea to use -d so no files are accidentally included. '
             'Also note that data files are made available as their name, not '
             'their full path, so name them uniquely.')
    parser.add_argument(
        '-d', '--custom-data-file',
        action='append', default=[],
        help='Specify data files to be made available in '
             'ProtocolContext.bundled_data (and possibly bundled if --bundle '
             'is passed). Can be specified multiple times with different '
             'files. It is usually a better idea to use this than -D because '
             'there is less possibility of accidentally including something.')
    parser.add_argument(
        '-o', '--outfile', type=pathlib.Path
        action='append', default=None,
        help='Optionally, specify output file for G-code export')
    parser.add_argument(
        '-p', '--port',
        action='append', default=pyotronext.configurator.Config.config["port"],
        help='Specify serial port, e.g. \'COM3\'.')
    parser.add_argument(
        'protocol', metavar='PROTOCOL',
        type=argparse.FileType('rb'),
        help='The protocol file to execute. If you pass \'-\', you can pipe '
        'the protocol via stdin; this could be useful if you want to use this '
        'utility as part of an automated workflow.')
    return parser

def main():
    """
    Handler for command line invocation to run a protocol.
    @param argv: The arguments the program was invoked with; this is usually
                 :py:attr:`sys.argv` but if you want to override that you can.
    @returns int: A success or failure value suitable for use as a shell
                  return code passed to :py:meth:`sys.exit` (0 means success,
                  anything else is a kind of failure).
    """
    parser = argparse.ArgumentParser(prog='pyotronext_execute',
                                     description='Run an OT-2 protocol')
    parser = get_arguments(parser)
    # don't want to add this in get_arguments because if somebody upstream is
    # using that parser they probably want their own version
    parser.add_argument(
        '-v', '--version', action='version', version=pyotronext.__version__)
    parser.add_argument(
        '-n', '--no-print-runlog', action='store_true',
        help='Do not print the commands as they are executed')
    args = parser.parse_args()
    printer = None
    if args.log_level != 'none':
        log_level = args.log_level
    else:
        log_level = 'warning'
    # Try to migrate containers from database to v2 format
    execute(args.protocol, args.protocol.name,
            log_level=log_level, emit_runlog=printer, outfile=args.outfile, direct_write=args.direct_write, direct_write_mode=args.direct_write_mode, port=args.port, baudrate=args.baudrate)
    return 0


if __name__ == '__main__':
    sys.exit(main())
