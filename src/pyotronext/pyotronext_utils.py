"""
Copyright 2018 - 2022 The pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 

SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/pyotronext_utils.py
@authors 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot running Marlin firmware.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md .
This file contains the utility functions getParentDirectory(), getResourcePath() and getResourcePathString().  
Language: Python 3.7
"""

import os  # provides fsdecode()
import pathlib  # provides Path()
import inspect  # provides getsourcefile()

def getParentDirectory():
    currentScriptDirectory = pathlib.Path(inspect.getsourcefile(lambda:0)).resolve().parent
    return currentScriptDirectory

def getResourcePath(subdirectory, resourceName):
    resourcesDirectory = getParentDirectory()
    subDirectoryPath = resourcesDirectory / subdirectory
    resourcePath = subDirectoryPath / resourceName
    return resourcePath

def getResourcePathString(resourceName):
    resourcePath = getResourcePath("resources", resourceName)
    resourcePathStr = os.fsdecode(resourcePath)
    return resourcePathStr
