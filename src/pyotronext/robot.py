"""
Copyright 2018  - 2020 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/hardware_control/pipette.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/robot.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other Pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The Pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert opentrons API 2 or mecode
compatible protocols into G-code (Marlin dialect) and communicate with the 
liquid handling robot running Marlin firmware.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
Language: Python 3.7
"""

import pyotronext.configurator
# import pyotronext.labwareItem

class VirtualRobot:
    config = pyotronext.configurator.Config.config

    tipRacks={}
    tipRacks10ulL=[]
    tipRacks20ulL=[]
    tipRacks200ulL=[]
    tipRacks1000ulL=[]
#    tipRack10uL=[tipRacks[Rack] for Rack in tipRacks if Rack.definition["parameters"]["load_name"] == "10µL"]
    """Internal counter/index for keeping current tip. Only change if you know what you are doing."""
    currentTipRack10ulL = 0
    currentTipRack20ulL = 0
    currentTipRack200ulL = 0
    currentTipRack1000ulL = 0
    currentTip10ulL = 0
    currentTip20ulL = 0
    currentTip200ulL = 0
    currentTip1000ulL = 0

    """Internal variable for keeping track of current z-hight. Do not change manually"""
    currentZhight = 0
    currentTipHight = 0
        
    currentLayout = None
    currentPlateHight = 0
    
    @classmethod
    def zOffset(cls):
        return cls.config["zWorkingSpaceOffset"] + cls.config["zToolOffset"] + cls.currentTipHight
    
    @classmethod
    def addTipRack(cls, rack):
#        TODO
        rackObj = next(iter(rack.values())).prnt
        rackName = rackObj.name 
        cls.tipRacks.update({rackName: rack})
        if rackObj.definition["parameters"]["loadName"] == "opentrons_96_tiprack_1000ul" or rackObj.definition["parameters"]["loadName"] == "geb_96_tiprack_1000ul":
            cls.tipRacks1000ulL.append(rackObj)
        elif  rackObj.definition["parameters"]["loadName"] == "opentrons_96_tiprack_300ul" or rackObj.definition["parameters"]["loadName"] == "tipone_96_tiprack_200ul":
            cls.tipRacks200ulL.append(rackObj)
        elif  rackObj.definition["parameters"]["loadName"] == "opentrons_96_tiprack_20ul":
            cls.tipRacks20ulL.append(rackObj)
        elif  rackObj.definition["parameters"]["loadName"] == "opentrons_96_tiprack_10ul" or rackObj.definition["parameters"]["loadName"] == "geb_96_tiprack_10ul":
            cls.tipRacks10ulL.append(rackObj)

    @classmethod
    def getNextTip(cls): #TODO add specific getNextTip<TipType>(cls)
        if cls.config["instrumentType"] == 'p1000_single':
            currentRackInstance = cls.tipRacks1000ulL[cls.currentTipRack1000ulL]
            currentTipInstance = currentRackInstance.vialInstances[currentRackInstance.vialLabelList[cls.currentTip1000ulL]]
            if cls.currentTip1000ulL >= len(currentRackInstance.vialInstances) - 1:
                if cls.currentTipRack1000ulL >= (len(cls.tipRacks1000ulL) - 1):
                    print("last tip is in use")
                else:
                    cls.currentTipRack1000ulL += 1
                    cls.currentTip1000ulL = 0
            else:
                cls.currentTip1000ulL += 1

        if cls.config["instrumentType"] == 'p300_single':
            currentRackInstance = cls.tipRacks300ulL[cls.currentTipRack300ulL]
            currentTipInstance = currentRackInstance.vialInstances[currentRackInstance.vialLabelList[cls.currentTip300ulL]]
            if cls.currentTip300ulL >= len(currentRackInstance.vialInstances) - 1:
                if cls.currentTipRack300ulL >= (len(cls.tipRacks300ulL) - 1):
                    print("last tip is in use")
                else:
                    cls.currentTipRack300ulL += 1
                    cls.currentTip300ulL = 0
            else:
                cls.currentTip300ulL += 1

        if cls.config["instrumentType"] == 'p100_single':
            currentRackInstance = cls.tipRacks100ulL[cls.currentTipRack100uL]
            currentTipInstance = currentRackInstance.vialInstances[currentRackInstance.vialLabelList[cls.currentTip100ulL]]
            if cls.currentTip100ulL >= len(currentRackInstance.vialInstances) - 1:
                if cls.currentTipRack100ulL >= (len(cls.tipRacks100ulL) - 1):
                    print("last tip is in use")
                else:
                    cls.currentTipRack100ulL += 1
                    cls.currentTip100ulL = 0
            else:
                cls.currentTip100ulL += 1

        if cls.config["instrumentType"] == 'p10_single':
            currentRackInstance = cls.tipRacks10ulL[cls.currentTipRack10ulL]
            currentTipInstance = currentRackInstance.vialInstances[currentRackInstance.vialLabelList[cls.currentTip10ulL]]
            if cls.currentTip10ulL >= len(currentRackInstance.vialInstances) - 1:
                if cls.currentTipRack10ulL >= (len(cls.tipRacks10ulL) - 1):
                    print("last tip is in use")
                else:
                    cls.currentTipRack10ulL += 1
                    cls.currentTip10ulL = 0
            else:
                cls.currentTip10ulL += 1

        nextTip = currentTipInstance
        return nextTip


    def __init__(self, robotID=0, 
                 instrumentModel="",
                 tipracks=None,
                 **kwargs
                 ):
        super().__init__()
        self.instrumentModel=instrumentModel
        if tipracks is not None:
            tipRacks = tipracks

#            if self.instrumentModel == "p1000_single":
                
#               cls.longRacks = {rack: tipRacks[rack] for Rack in len(tipRacks) if (Racks.loadName == "p1000L")}
#               self.shortRacks= [tipRacks[Rack] for Rack in len(tipRacks) if (Racks.loadName == "p1000_single")]
#               self.shortTips = [shortRacks[sR].wells for sR in range(0, shortRacks.length())] 
#               self.longTips = [longRacks[lR].wells for lR in range(0, longRacks.length())] 
            
#               self.currentRow1000uL=shortTips[0]
#               self.currentCol1000uL=shortTips[0]

    
