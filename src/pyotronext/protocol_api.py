"""
Copyright 2018  - 2020 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/contexts.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/protocol_api.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/contexts.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
This file contains the ProtocolContext class that serves as a replacement for the 
protocol context class of the opentrons API version 2.  
Language: Python 3.7
"""

import pathlib
import json

import pyotronext.pyotronext_utils
import pyotronext.configurator
import pyotronext.virtualLab
import pyotronext.labwareItem
import pyotronext.protocols.types


MAX_SUPPORTED_VERSION = pyotronext.protocols.types.APIVersion(2, 1)

class ProtocolContext:
    """
    This is the one and only representation of the complete lab and serves as 
    the API. users should only control the robot via this API.
    Exactly one instance of this class must be created per physical hardware unit 
    that is to be controlled by a program/protocol. Use as a replacement for
    opentrons's ProtocolContext. Example:
    
    ```(.py)
    import pyotronext.execute
    from pytronext import protocol_api 

    def run(protocol: protocol_api.ProtocolContext):
        
        tiprack1 = protocol.load_labware(load_name="gep_96_tiprack_10ul, slot=1")
        plate1 = protocol.load_labware(load_name="corning_96_wellplate_360ul_flat", slot=1)
    
        pipette = protocol.load_instrument(instrument_name='p1000_single', mount='left', tip_racks=[tiprack1])
        
        pipette.move_to(plate1["A1"])
        pipette.move_to(plate1["A2"].top(z=5)) # move 5mm above top of plate1, well "A2"
        
        pipette.teardown()

    protocol = pyotronext.execute.get_protocol_api(version="2.0")  # returns a ProtocolContext(..., api_version=(2, 0)) 

    
    run(protocol)
    ```
    """
    
    def __init__(self, 
                robotID=1,  
                direct_write=True, 
                direct_write_mode="serial", 
                outfile=None, 
                port=pyotronext.configurator.Config.config["port"], 
                baudrate=pyotronext.configurator.Config.config["baudrate"],
                api_version=None, 
                **kwargs
                ):
        
        self.robotID = robotID
        self.direct_write=direct_write
        self.direct_write_mode=direct_write_mode
        self.outfile=outfile
        self.port=port
        self.baudrate=baudrate
        self.nextLabwareID = 1
        self.currentScriptDirectory = pyotronext.pyotronext_utils.getParentDirectory()
        self.api_version=api_version
        self._api_version = api_version or MAX_SUPPORTED_VERSION
        if self._api_version > MAX_SUPPORTED_VERSION:
            raise RuntimeError(
                f'API version {self._api_version} is not supported by this '
                f'robot software. Please either reduce your requested API '
                f'version or update your robot.')
        
    def api_version(self):
        return self.api_version or MAX_SUPPORTED_VERSION
    
    def load_labware(self, load_name="", location=None, label=None, namespace=None, version=1):
        """
        Add labware to VirtualLab aka instrument
        """
        definitionFileName = f"{load_name}.json" 
        definitionFilePath = pyotronext.pyotronext_utils.getResourcePath("labware", definitionFileName)
        with definitionFilePath.open() as definitionFileHandler: 
            labwareDefinition = json.load(definitionFileHandler)
        labwareObj = pyotronext.labwareItem.LabwareItem(definition = labwareDefinition, IDnum = self.nextLabwareID, slot = int(location))
        name = load_name + str(labwareObj.IDnum)
        pyotronext.virtualLab.VirtualLab.addLabware(definition=labwareDefinition, IDnum=self.nextLabwareID, slot=int(location))
        self.nextLabwareID += 1
        return labwareObj.vialInstances
    
    def load_instrument(self, instrument_name="", mount='left', tip_racks=None):
        instrument = pyotronext.virtualLab.VirtualLab(robotID=self.robotID, 
                                                      direct_write=self.direct_write, 
                                                      direct_write_mode=self.direct_write_mode, 
                                                      outfile=self.outfile, 
                                                      port=self.port, 
                                                      baudrate=self.baudrate, 
                                                      instrument_name=instrument_name, 
                                                      tipRacks=tip_racks
                                                      )
        return instrument 
    
    def home(self):
        # pyotronext.virtualLab.VirtualLab.home()
        return
