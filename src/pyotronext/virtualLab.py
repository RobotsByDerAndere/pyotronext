"""
Copyright 2018  - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/hardware_control/__init__.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/virtualLab.py
@package pyotronext
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The Pyotronext authors 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/hardware_control/__init__.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0 (https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert opentrons API 2 or mecode
compatible protocols into G-code (Marlin dialect) and communicate with the 
liquid handling robot running Marlin firmware.
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
Language: Python 3.7
"""

"""
This software is based on third-party software. Each third-party software is 
licensed under the terms of its separate license. The License text 
can be found in the directory 3rd_party_licenses/ . The information in this 
comment block pertains to the respective third-party software. 

mecode (https://github.com/jminardi/mecode)
Copyright (c) 2014 Jack Minardi
License: MIT
Contributions: mecode was developed by the Lewis Lab at Harvard University.

opentrons (https://github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0
SPDX-License-Identifyier: Apache-2.0
"""

import math
from collections import defaultdict
import pathlib  # provides Path() and Path().write_text(), Path().resolve() and Path().resolve().Parent() for file handling. Replaces os.path and does not need open()

import pyotronext.pyotronext_utils
import pyotronext.serialCommunication
import pyotronext.labwareItem
import pyotronext.robot

currentScriptDirectory = pyotronext.pyotronext_utils.getParentDirectory()

class Clearance:
    uptake = 2
    disp = 5

class VirtualLab:
    """
    This is the one and only representation of the complete lab and serves as 
    the API. users should only control the robot via this API.
    Exactly one instance of this class must be created per physical hardware unit 
    that is to be controlled by a program/protocol.
    """
    labwareObjects = {}
    wasteObjects = []

    def __init__(self, 
                 robotID=0,
                 outfile=None, 
                 print_lines=True, 
                 header=None, 
                 footer=None,
                 aerotech_include=True,
                 output_digits=6,
                 direct_write=True,
                 direct_write_mode='serial',
                 printer_host='localhost',
                 port=pyotronext.robot.VirtualRobot.config["port"],
                 baudrate=pyotronext.robot.VirtualRobot.config["baudrate"],
                 two_way_comm=True,
                 x_axis='X',
                 y_axis='Y',
                 z_axis='Z',
                 i_axis=pyotronext.robot.VirtualRobot.config["axis4Name"],
                 j_axis=pyotronext.robot.VirtualRobot.config["axis5Name"],
                 k_axis=pyotronext.robot.VirtualRobot.config["axis6Name"],
                 extrude=False,
                 filament_diameter=1.00,
                 layer_height=0.19,
                 extrusion_width=0.35,
                 extrusion_multiplier=1,
                 setup=True,
                 lineend='os',
                 instrument_name='',
                 tipRacks=None):
        """
        Parameters
        ----------
        outfile : path or None (default: None)
            If a path is specified, the compiled gcode will be writen to that
            file.
        print_lines : bool (default: True)
            Whether or not to print the compiled GCode to stdout
        header : path or None (default: None)
            Optional path to a file containing lines to be written at the
            beginning of the output file
        footer : path or None (default: None)
            Optional path to a file containing lines to be written at the end
            of the output file.
        aerotech_include : bool (default: True)
            If true, add aerotech specific functions and var defs to outfile.
        output_digits : int (default: 6)
            How many digits to include after the decimal in the output gcode.
        direct_write : bool (default: False)
            If True a socket or serial port is opened to the printer and the
            GCode is sent directly over.
        direct_write_mode : str (either 'socket' or 'serial') (default: socket)
            Specify the channel your printer communicates over, only used if
            `direct_write` is True.
        printer_host : str (default: 'localhost')
            Hostname of the printer, only used if `direct_write` is True.
        port : int (default: 8000)
            Port of the printer, only used if `direct_write` is True.
        baudrate: int (default: 250000)
            The baudrate to connect to the printer with.
        two_way_comm : bool (default: True)
            If True, mecode waits for a response after every line of GCode is
            sent over the socket. The response is returned by the `write`
            method. Only applies if `direct_write` is True.
        x_axis : str (default 'X')
            The name of the x axis (used in the exported gcode)
        y_axis : str (default 'Y')
            The name of the z axis (used in the exported gcode)
        z_axis : str (default 'Z')
            The name of the z axis (used in the exported gcode)
        i_axis : str (default 'U')
            The name of the i axis (used in the exported gcode)
        j_axis : str (default 'V')
            The name of the j axis (used in the exported gcode)
        k_axis : str (default 'W')
            The name of the k axis (used in the exported gcode)
        extrude : True or False (default: False)
            If True, a flow calculation will be done in the move command. The
            neccesary length of filament to be pushed through on a move command
            will be tagged on as a kwarg. ex. X5 Y5 E3
        filament_diameter: float (default 1.75)
            the diameter of FDM filament you are using
        layer_height : float
            Layer height for FDM printing. Only relavent when extrude = True.
        extrusion width: float
            total width of the capsule shaped cross section of a squashed filament.
        extrusion_multiplier: float (default = 1)
            The length of extrusion filament to be pushed through on a move
            command will be multiplied by this number before being applied.
        setup : Bool (default: True)
            Whether or not to automatically call the setup function.
        lineend : str (default: 'os')
            Line ending to use when writing to a file or printer. The special
            value 'os' can be passed to fall back on python's automatic
            lineending insertion.

        """
        super().__init__()
        self.well_bottom_clearance = Clearance()
        self.virtualRobot = pyotronext.robot.VirtualRobot(robotID=robotID, instrumentModel=instrument_name, tipRacks=tipRacks)
        self.config = pyotronext.robot.VirtualRobot.config
        self.instrumenttype=instrument_name
        if tipRacks is not None:
            for rack in tipRacks:
                pyotronext.robot.VirtualRobot.addTipRack(rack)
        
        self.default_speed = self.config["xySpeeds"][2]  # in mm/min
        self.outfile = outfile
        self.print_lines = print_lines
        self.header = header
        self.footer = footer
        self.aerotech_include = aerotech_include
        self.output_digits = output_digits
        self.direct_write = direct_write
        self.direct_write_mode = direct_write_mode
        self.printer_host = printer_host
        self.port = port
        self.baudrate = baudrate
        self.two_way_comm = two_way_comm
        self.x_axis = x_axis
        self.y_axis = y_axis
        self.z_axis = z_axis
        self.i_axis = i_axis
        self.j_axis = j_axis
        self.k_axis = k_axis

        self._current_position = defaultdict(float)
        self.is_relative = False

        self.extrude = extrude
        self.filament_diameter = filament_diameter
        self.layer_height = layer_height
        self.extrusion_width = extrusion_width
        self.extrusion_multiplier = extrusion_multiplier

        self.position_history = [(0, 0, 0, 0)]
        self.speed = 0
        self.speed_history = []

        self._socket = None
        self._p = None

        # If the user passes in a line ending then we need to open the output
        # file in binary mode, otherwise python will try to be smart and
        # convert line endings in a platform dependent way.
        if lineend == 'os':
            mode = 'w+'
            self.lineend = '\n'
        else:
            mode = 'wb+'
            self.lineend = lineend

        if isinstance(outfile, pathlib.Path)
            self.out_fd = outfile.open(mode)
        if isinstance(outfile, str):
            self.out_fd = open(outfile, mode)
        elif outfile is not None:  # if outfile not str assume it is an open file
            self.out_fd = outfile
        else:
            self.out_fd = None

        if setup:
            self.setup()

    @property
    def current_position(self):
        return self._current_position

    def __enter__(self):
        """
        Context manager entry
        Can use like:

        with mecode.G(  outfile=self.outfile,
                        print_lines=False,
                        aerotech_include=False) as g:
            <code block>
        """
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Context manager exit
        """
        self.teardown()

    # GCode Aliases  ########################################################

    def set_home(self, x=None, y=None, z=None, i=None, **kwargs):
        """ Set the current position to the given position without moving.

        Example
        -------
        >>> # set the current position to X=0, Y=0
        >>> g.set_home(0, 0)

        """
        args = self._format_args(x, y, z, i, **kwargs)
        space = ' ' if len(args) > 0 else ''
        self.write('G92' + space + args + ' ;set home');

        self._update_current_position(mode='absolute', x=x, y=y, z=z, i=i, **kwargs)

    def reset_home(self):
        """ Reset the position back to machine coordinates without moving.
        """
        # FIXME This does not work with internal current_position
        # FIXME You must call an abs_move after this to re-sync
        # current_position
        self.write('G92.1 ;reset position to machine coordinates without moving')

    def relative(self):
        """ Enter relative movement mode, in general this method should not be
        used, most methods handle it automatically.

        """
        if not self.is_relative:
            self.write('G91 ;relative')
            self.is_relative = True

    def absolute(self):
        """ Enter absolute movement mode, in general this method should not be
        used, most methods handle it automatically.

        """
        if self.is_relative:
            self.write('G90 ;absolute')
            self.is_relative = False

    def feed(self, rate):
        """ Set the feed rate (tool head speed) in (typically) mm/minute

        Parameters
        ----------
        rate : float
            The speed to move the tool head in (typically) mm/minute.

        """
        self.write('G1 F{}'.format(rate))
        self.speed = rate

    def dwell(self, time):
        """ Pause code executions for the given amount of time.

        Parameters
        ----------
        time : float
            Time in milliseconds to pause code execution.

        """
        self.write('G4 P{}'.format(time))

    # Composed Functions  #####################################################

    def setup(self):
        """ Set the environment into a consistent state to start off. This
        method must be called before any other commands.

        """
        self._write_header()
        if self.is_relative:
            self.write('G91 ; relative')
        else:
            self.write('G90 ; absolute')
        self.write('G28 ; home all axes') 
        self.write('G90 ; absolute') 

    def teardown(self, wait=True):
        """ Close the outfile file after writing the footer if opened. This
        method must be called once after all commands.

        Parameters
        ----------
        wait : Bool (default: True)
            Only used if direct_write_model == 'serial'. If True, this method
            waits to return until all buffered lines have been acknowledged.

        """

        if self.out_fd is not None:
            if self.aerotech_include is True:
                with open(currentScriptDirectory / 'footer.txt') as fd:
                    self._write_out(lines=fd.readlines())
            if self.footer is not None:
                with open(self.footer) as fd:
                    self._write_out(lines=fd.readlines())
            if self.outfile is None:
                self.out_fd.close()
        if self._socket is not None:
            self.write('M400')
            self._socket.close()
        if self._p is not None:
            self.write('M400')
            self._p.disconnect(wait)

    def home(self):
        """ Move the tool head to the home position (X=0, Y=0).
        """
        self.abs_move(x=0, y=0)

    def move(self, x=None, y=None, z=None, i=None, rapid=False, **kwargs):
        """ Move the tool head to the given position. This method operates in
        relative mode unless a manual call to `absolute` was given previously.
        If an absolute movement is desired, the `abs_move` method is
        recommended instead.

        Examples
        --------
        >>> # move the tool head 10 mm in x and 10 mm in y
        >>> virtualLab.move(x=10, y=10)
        >>> # the x, y, and z keywords may be omitted:
        >>> virtualLab.move(10, 10, 10)

        >>> # move the U axis up 20 mm
        >>> virtualLab.move(U=20)

        """
        if self.extrude is True and 'E' not in kwargs.keys():
            if self.is_relative is not True:
                x_move = self._current_position['x'] if x is None else x
                y_move = self._current_position['y'] if y is None else y
                x_distance = abs(x_move - self._current_position['x'])
                y_distance = abs(y_move - self._current_position['y'])
                current_extruder_position = self._current_position['E']
            else:
                x_distance = 0 if x is None else x
                y_distance = 0 if y is None else y
                current_extruder_position = 0
            line_length = math.sqrt(x_distance**2 + y_distance**2)
            area = self.layer_height*(self.extrusion_width-self.layer_height) + \
                3.14159*(self.layer_height/2)**2
            volume = line_length*area
            filament_length = ((4*volume)/(3.14149*self.filament_diameter**2))*self.extrusion_multiplier
            kwargs['E'] = filament_length + current_extruder_position
        if z is not None:
            z = z + pyotronext.robot.VirtualRobot.zOffset()
            pyotronext.robot.VirtualRobot.currentZhight = z
        if (x is not None and x != self._current_position['x']) or (y is not None and y != self._current_position['y']) or (z is not None and z != self._current_position['z']) or (i is not None and i != self._current_position['i']) or 'E' in kwargs.keys():
            self._update_current_position(x=x, y=y, z=z, i=i, **kwargs)
            args = self._format_args(x, y, z, i, **kwargs)
            cmd = 'G0 ' if rapid else 'G1 '
            self.write(cmd + args)

    def abs_move(self, x=None, y=None, z=None, i=None, rapid=False, **kwargs):
        """ Same as `move` method, but positions are interpreted as absolute.
        """
        if self.is_relative:
            self.absolute()
            self.move(x=x, y=y, z=z, rapid=rapid, **kwargs)
            self.relative()
        else:
            self.move(x=x, y=y, z=z, rapid=rapid, **kwargs)

    def rapid(self, x=None, y=None, z=None, i=None, **kwargs):
        """ Executes an uncoordinated move to the specified location.
        """
        self.move(x, y, z, rapid=True, **kwargs)

    def abs_rapid(self, x=None, y=None, z=None, i=None, **kwargs):
        """ Executes an uncoordinated abs move to the specified location.
        """
        self.abs_move(x, y, z, i, rapid=True, **kwargs)

    def retract(self, retraction):
        if self.extrude is False:
            self.move(E = -retraction)
        else:
            self.extrude = False
            self.move(E = -retraction)
            self.extrude = True


    # Public Interface  #######################################################

    def view(self, backend='mayavi'):
        """ View the generated Gcode.

        Parameters
        ----------
        backend : str (default: 'matplotlib')
            The plotting backend to use, one of 'matplotlib' or 'mayavi'.

        """
        import numpy as np
        history = np.array(self.position_history)

        if backend == 'matplotlib':
            from mpl_toolkits.mplot3d import Axes3D
            import matplotlib.pyplot as plt
            fig = plt.figure()
            ax = fig.gca(projection='3d')
            ax.set_aspect('equal')
            X, Y, Z = history[:, 0], history[:, 1], history[:, 2]
            ax.plot(X, Y, Z)

            # Hack to keep 3D plot's aspect ratio square. See SO answer:
            # http://stackoverflow.com/questions/13685386
            max_range = np.array([X.max()-X.min(),
                                  Y.max()-Y.min(),
                                  Z.max()-Z.min()]).max() / 2.0

            mean_x = X.mean()
            mean_y = Y.mean()
            mean_z = Z.mean()
            ax.set_xlim(mean_x - max_range, mean_x + max_range)
            ax.set_ylim(mean_y - max_range, mean_y + max_range)
            ax.set_zlim(mean_z - max_range, mean_z + max_range)

            plt.show()
        elif backend == 'mayavi':
            from mayavi import mlab
            mlab.plot3d(history[:, 0], history[:, 1], history[:, 2])
        else:
            raise Exception("Invalid plotting backend! Choose one of mayavi or matplotlib.")

    def write(self, statement_in, resp_needed=False):
        if self.print_lines:
            print(statement_in)
        self._write_out(statement_in)
        statement = (statement_in + self.lineend).encode(encoding="UTF-8")
        if self.direct_write is True:
            if self.direct_write_mode == 'socket':
                if self._socket is None:
                    import socket
                    self._socket = socket.socket(socket.AF_INET,
                                                socket.SOCK_STREAM)
                    self._socket.connect((self.printer_host, self.port))
                self._socket.send(statement)
                if self.two_way_comm is True:
                    response = self._socket.recv(8192)
                    response = response.decode(encoding="UTF-8")
                    if response[0] != '%':
                        raise RuntimeError(response)
                    return response[1:-1]
            elif self.direct_write_mode == 'serial':
                print("direct_write")
                if self._p is None:
                    self._p = pyotronext.serialCommunication.Communication(self.port, self.baudrate)
                    self._p.connect()
                    self._p.start()
                if resp_needed:
                    return self._p.get_response(statement_in)
                else:
                    self._p.sendline(statement_in)

    def rename_axis(self, x=None, y=None, z=None, i=None):
        """ Replaces the x, y, z, or i axis with the given name.

        Examples
        --------
        >>> g.rename_axis(z='A')

        """
        if x is not None:
            self.x_axis = x
        elif y is not None:
            self.y_axis = y
        elif z is not None:
            self.z_axis = z
        elif i is not None:
            self.i_axis = i
        else:
            msg = 'Must specify new name for x, y, z, or i only'
            raise RuntimeError(msg)

    # Private Interface  ######################################################

    def _write_out(self, line=None, lines=None):
        """ Writes given `line` or `lines` to the output file.
        """
        # Only write if user requested an output file.
        if self.out_fd is None:
            return

        if lines is not None:
            for line in lines:
                self._write_out(line)

        line = line.rstrip() + self.lineend  # add lineend character
        if hasattr(self.out_fd, 'mode') and 'b' in self.out_fd.mode:  # encode the string to binary if needed
            line = line.encode(encoding="UTF-8")
        self.out_fd.write(line)

    def _write_header(self):
        if self.aerotech_include is True:
            with open(currentScriptDirectory / 'header.txt') as fd:
                self._write_out(lines=fd.readlines())
        if self.header is not None:
            with open(self.header) as fd:
                self._write_out(lines=fd.readlines())

    def _format_args(self, x=None, y=None, z=None, i=None, j=None, k=None, **kwargs):
        d = self.output_digits
        args = []
        if x is not None:
            args.append('{0}{1:.{digits}f}'.format(self.x_axis, x, digits=d))
        if y is not None:
            args.append('{0}{1:.{digits}f}'.format(self.y_axis, y, digits=d))
        if z is not None:
            args.append('{0}{1:.{digits}f}'.format(self.z_axis, z, digits=d))
        if i is not None:
            args.append('{0}{1:.{digits}f}'.format(self.i_axis, i, digits=d))
        if j is not None:
            args.append('{0}{1:.{digits}f}'.format(self.j_axis, j, digits=d))
        if k is not None:
            args.append('{0}{1:.{digits}f}'.format(self.k_axis, k, digits=d))
        args += ['{0}{1:.{digits}f}'.format(k, kwargs[k], digits=d) for k in sorted(kwargs)]
        args = ' '.join(args)
        return args

    def _update_current_position(self, mode='auto', x=None, y=None, z=None, i=None, 
                                 **kwargs):
        if mode == 'auto':
            mode = 'relative' if self.is_relative else 'absolute'

        if self.x_axis != 'X' and x is not None:
            kwargs[self.x_axis] = x
        if self.y_axis != 'Y' and y is not None:
            kwargs[self.y_axis] = y
        if self.z_axis != 'Z' and z is not None:
            kwargs[self.z_axis] = z
        if self.i_axis != pyotronext.robot.VirtualRobot.config["axis4Name"] and i is not None:
            kwargs[self.i_axis] = i

        if mode == 'relative':
            if x is not None:
                self._current_position['x'] = self._current_position['x'] + x
            if y is not None:
                self._current_position['y'] = self._current_position['y'] + y
            if z is not None:
                self._current_position['z'] = self._current_position['z'] + z
            if i is not None:
                self._current_position['i'] = self._current_position['i'] + i
            for dimention, delta in kwargs.items():
                self._current_position[dimention] = self._current_position[dimention] + delta
        else:
            if x is not None:
                self._current_position['x'] = x
            if y is not None:
                self._current_position['y'] = y
            if z is not None:
                self._current_position['z'] = z
            if i is not None:
                self._current_position['i'] = i

            for dimention, delta in kwargs.items():
                self._current_position[dimention] = delta

        x = self._current_position['x']
        y = self._current_position['y']
        z = self._current_position['z']
        i = self._current_position['i']

        self.position_history.append((x, y, z, i))

        len_history = len(self.position_history)
        if (len(self.speed_history) == 0
            or self.speed_history[-1][1] != self.speed):
            self.speed_history.append((len_history - 1, self.speed))
        
    def move_to(self, location=None):
        if type(location)==tuple:
            x = location[0]
            y = location[1]
            z = location[2]
        else:
            x = location.xPos
            y = location.yPos
            z = location.plateHight + 5 
        currentZ = pyotronext.robot.VirtualRobot.currentZhight
        currentPlateHight = pyotronext.robot.VirtualRobot.currentPlateHight
        maxObstacleZ = max(currentPlateHight + 5, z)
        if type(location) == tuple:
            if (currentZ < maxObstacleZ and (x != self._current_position['x'] or y != self._current_position['y'])):
                self.move(z=self.config["zSizeWorkingSpace"] - pyotronext.robot.VirtualRobot.zOffset() - 5, F= + self.config["zSpeeds"][2])
        currentZ = pyotronext.robot.VirtualRobot.currentZhight
        if (currentZ < maxObstacleZ and (x != self._current_position['x'] or y != self._current_position['y'])):
            self.move(z=maxObstacleZ, F= + self.config["zSpeeds"][2])
        currentZ = pyotronext.robot.VirtualRobot.currentZhight
        if (currentZ < z):
            self.move(z=z, F= + self.config["zSpeeds"][2])
        self.move(x=x, y=y, F= self.default_speed)
        if (currentZ > z):
            self.move(z=z, F= + self.config["zSpeeds"][2])
        if type(location) is not tuple or (x != self._current_position['x'] or y != self._current_position['y']): 
            pyotronext.robot.VirtualRobot.currentPlateHight = z

    
    def pick_up_tip(self, location=None):
        if pyotronext.robot.VirtualRobot.currentTipHight == 0:
            if location is None:
                location = self.virtualRobot.getNextTip()
            if location.prnt.tipLength != 0:
                self.move_to(location=location)
                self.move_to(location=location.bottom())
                self.move_to(location=location.top(z=5))
                self.move_to(location=location.bottom())
                self.move(z=self.config["zSizeWorkingSpace"] - pyotronext.robot.VirtualRobot.zOffset() - 5, F=self.config["zSpeeds"][2])
                pyotronext.robot.VirtualRobot.currentTipHight = location.prnt.tipLength
            else: 
                print("error: last call of pick_up_tip(location) did nothing because location was not a tipRack")
        else:
            print("error: last call of pick_up_tip() did nothing because tip is already attached")

    def drop_tip(self, location=None):
        if location is None:
            location = self.wasteObjects[0].vialInstances["A1"]
        self.move_to(location)
        self.move_to(location=location.bottom())
        self.move_to(location=location.top())
        self.move_to(location=location.bottom())
        self.move_to(location=location.top(z=5))
        pyotronext.robot.VirtualRobot.currentTipHight = 0
        
    def aspirate(self, volume=0.0, location=None):
        if location is not None:
            self.move_to(location)
            if type(location) is not tuple:
                self.move_to(location.bottom(z=self.well_bottom_clearance.uptake))
        if not self.is_relative:
            self.relative()
        self.move(i=volume, F=self.config["dispenseSpeeds"][1])
        self.absolute()
        if ((location is not None) and (type(location) is not tuple)):
            self.move(z=location.top(z=5))

    def dispense(self, volume=0.0, location=None):
        if location is not None:
            self.move_to(location)
        if not self.is_relative:
            self.relative()
        self.move(i=-volume, F=self.config["dispenseSpeeds"][1])
        self.absolute()

    def distribute(self, volume=0.0, source=None, dest=None):
        if dest is not None:
            v = len(dest) * volume
        if source is not None:
            self.aspirate(volume = v, source=source) 
        if dest is not None:
            if type(dest) is list:
                for i in range(0, len(dest)-1):
                    self.dispense(volume=volume, dest=dest[i])
            else: self.dispense(volume=volume, location=dest)
            
    def transfer(self, volume=None, source=None, dest=None):
        if source is not None:
            if type(source) is list:
                for i in range(0, len(source)-1):
                    self.aspirate(volume=volume[i], source=source[i])
                    self.dispense(volume=volume[i], dest=dest[i])
            else: self.distribute(volume=volume, source=source, dest=dest)
    
    def pause(self):
        time = 10000
        self.dwell(time)
    
    def delay(self, minutes=0.0, seconds=0.0):
        time = minutes * 60 + seconds 
        self.dwell(time)

    
    def mix(self, repetitions=1, volume=None, location=None):
        if volume is None:
            if abs(pyotronext.robot.VirtualRobot.currentTipHight - self.config["tipHight10ulL"]) < 5:
                volume = 10
            elif abs(pyotronext.robot.VirtualRobot.currentTipHight - self.config["tipHight200ulL"]) < 5 or abs(pyotronext.robot.VirtualRobot.currentTipHight - self.config["tipHight200ulL"] - 100) < 5:
                volume = 200
            elif abs(pyotronext.robot.VirtualRobot.currentTipHight - self.config["tipHight1000ulL"]) < 5: 
                volume = 1000
            else:
                volume = 0      
            for i in range(repetitions):
                self.aspirate(volume=volume, location=location)
                self.dispense(volume=volume, location=location)

    def blow_out(self, location=None):
        if location is not None:
            self.move_to(location)
        self.move(E=0, F=self.config["dispenseSpeeds"][1])

    @classmethod
    def addLabware(cls, definition=None, IDnum=0, slot=None):
        labwareObj = pyotronext.labwareItem.LabwareItem(definition = definition, IDnum = IDnum, slot = int(slot))
        name = definition["parameters"]["loadName"] + str(labwareObj.IDnum)
        cls.labwareObjects.update({name: labwareObj})
        if definition["parameters"]["format"] == "trash":
            cls.wasteObjects.append(labwareObj)

    """
    @classmethod
    def home(cls, definition=None):
        cls.write('G28 ; home all axes')
    """
