"""
Copyright 2018 - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
Based on (with modifications) https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/src/ggcgen/configurator.py)
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/configurator.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other Pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot running Marlin firmware.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
This file contains the class Config that contains the parameters from the 
config.INI file.  
Language: Python 3.7
"""

import pathlib

import configobj

import pyotronext.pyotronext_utils

class Config:
    currentScriptDirectory = pyotronext.pyotronext_utils.getParentDirectory()
    configFileName = "config.ini"
    configFilePath = currentScriptDirectory / configFileName
    with configFilePath.open() as configFileHandler:
        config = configobj.ConfigObj(infile=configFileHandler, unrepr=True)
