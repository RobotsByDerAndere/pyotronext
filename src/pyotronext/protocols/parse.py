"""
Copyright 2018  - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocols/parse.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/protocols/parse.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other Pyortronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018-2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS) 
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
This file contains the parse() function for opentrons APIv2 Python protocols.  
Language: Python 3.7
"""

"""
This software is based on third-party software. Each third-party software is 
licensed under the terms of its separate license. The License text
can be found in the directory 3rd_party_licenses/ . The information in this
comment block pertains to the respective third-party software.

opentrons (https://github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0
"""

import ast
import re
import itertools

import pyotronext.protocols.types

# match e.g. "2.0" but not "hi", "2", "2.0.1"
API_VERSION_RE = re.compile(r'^(\d+)\.(\d+)$')


def version_from_string(vstr):
    """ Parse an API version from a string
    :param str vstr: The version string to parse
    :returns APIVersion: The parsed version
    :raises ValueError: if the version string is the wrong format
    """
    matches = API_VERSION_RE.match(vstr)
    if not matches:
        raise ValueError(
            f'apiLevel {vstr} is incorrectly formatted. It should '
            'major.minor, where both major and minor are numbers.')
    return pyotronext.protocols.types.APIVersion(
        major=int(matches.group(1)), minor=int(matches.group(2)))



def extract_metadata(parsed):
    metadata = {}
    assigns = [
        obj for obj in parsed.body if isinstance(obj, ast.Assign)]
    for obj in assigns:
        # XXX This seems brittle and could probably do with
        # - enough love that we can remove the type: ignores
        # - some thought about exactly what types are allowed in metadata
        if isinstance(obj.targets[0], ast.Name) \
                and obj.targets[0].id == 'metadata' \
                and isinstance(obj.value, ast.Dict):
            keys = [k.s for k in obj.value.keys]  # type: ignore
            values = [v.s for v in obj.value.values]  # type: ignore
            metadata = dict(zip(keys, values))
    return metadata


def _parse_python(
    protocol_contents=None,
    filename=None,
    bundled_labware= None,
    bundled_data= None,
    bundled_python= None,
    extra_labware= None,
):
    """ Parse a protocol known or at least suspected to be python """
    filename_checked = filename or '<protocol>'
    if filename_checked.endswith('.zip'):
        ast_filename = 'protocol.ot2.py'
    else:
        ast_filename = filename_checked

    parsed = ast.parse(protocol_contents,
                       filename=ast_filename)

    metadata = extract_metadata(parsed)
    protocol = compile(parsed, filename=ast_filename, mode='exec')
    version = get_version(metadata, parsed)

    result = pyotronext.protocols.types.PythonProtocol(
        text=protocol_contents,
        filename=getattr(protocol, 'co_filename', '<protocol>'),
        contents=protocol,
        metadata=metadata,
        api_level=version,
        bundled_labware=bundled_labware,
        bundled_data=bundled_data,
        bundled_python=bundled_python,
        extra_labware=extra_labware)

    return result



def parse(
    protocol_file=None,
    filename=None,
    extra_labware=None,
    extra_data=None
):
    """ Parse a protocol from text.
    :param protocol_file: The protocol file, or for single-file protocols, a
                        string of the protocol contents.
    :param filename: The name of the protocol. Optional, but helps with
                     deducing the kind of protocol (e.g. if it ends with
                     '.json' we can treat it like json)
    :param extra_labware: Any extra labware defs that should be given to the
                          protocol. Ignored if the protocol is json or zipped
                          python.
    :param extra_data: Any extra data files that should be provided to the
                       protocol. Ignored if the protocol is json or zipped
                       python.
    :return types.Protocol: The protocol holder, a named tuple that stores the
                        data in the protocol for later simulation or
                        execution.
    """
    if filename and filename.endswith('.zip'):
        if not isinstance(protocol_file, bytes):
            raise RuntimeError('Please update your Run App version to '
                               'support uploading a .zip file')
    else:
        if isinstance(protocol_file, bytes):
            protocol_str = protocol_file.decode('utf-8')
        else:
            protocol_str = protocol_file

        if filename and filename.endswith('.py'):
            return _parse_python(
                protocol_str, filename, extra_labware=extra_labware,
                bundled_data=extra_data)
            
def infer_version_from_imports(parsed):
    # Imports in the form of `import opentrons.robot` will have an entry in
    # parsed.body[i].names[j].name in the form "opentrons.robot". Find those
    # imports and transform them to strip away the 'opentrons.' part.
    ot_imports = ['.'.join(name.name.split('.')[1:]) for name in
                  itertools.chain.from_iterable(
                      [obj.names for obj in parsed.body
                       if isinstance(obj, ast.Import)])
                  if ('opentrons' in name.name or 'pyotronext' in name.name)]

    # Imports in the form of `from opentrons import robot` (with or without an
    # `as ___` statement) will have an entry in parsed.body[i].module
    # containing "opentrons"
    ot_from_imports = [
        name.name for name in
        itertools.chain.from_iterable(
            [obj.names for obj in parsed.body
             if isinstance(obj, ast.ImportFrom)
             and obj.module
             and ('opentrons' in obj.module or 'pyotronext' in obj.module)])
    ]

    # If any of these are populated, filter for entries with v1-specific terms
    opentrons_imports = set(ot_imports + ot_from_imports)
    v1_markers = set(('robot', 'instruments', 'modules', 'containers'))
    v1evidence = v1_markers.intersection(opentrons_imports)
    if v1evidence:
        return pyotronext.protocols.types.APIVersion(1, 0)
    else:
        raise RuntimeError('Cannot infer API level')


def version_from_metadata(metadata):
    """ Build an API version from metadata, if we can.
    If there is no apiLevel key, raise a KeyError.
    If the apiLevel value is malformed, raise a ValueError.
    """
    if 'apiLevel' not in metadata:
        raise KeyError('apiLevel')
    requested_level = str(metadata['apiLevel'])
    if requested_level == '1':
        return pyotronext.protocols.types.APIVersion(1, 0)

    return version_from_string(requested_level)


def get_version(metadata: pyotronext.protocols.types.Metadata, parsed: ast.Module) -> pyotronext.protocols.types.APIVersion:
    """
    Infer protocol API version based on a combination of metadata and imports.
    If a protocol specifies its API version using the 'apiLevel' key of a top-
    level dict variable named `metadata`, the value for that key will be
    returned as the version (the value will be intified, so numeric values
    only can be used).
    If that variable does not exist or if it does not contain the 'apiLevel'
    key, the API version will be inferred from the imports. A script with an
    import containing 'robot', 'instruments', or 'modules' will be assumed to
    be an APIv1 protocol. If none of these are present, it is assumed to be an
    APIv2 protocol (note that 'labware' is not in this list, as there is a
    valid APIv2 import named 'labware').
    """
    try:
        return version_from_metadata(metadata)
    except KeyError:  # No apiLevel key, may be apiv1
        pass
    try:
        return infer_version_from_imports(parsed)
    except RuntimeError:
        raise RuntimeError(
            'If this is not an API v1 protocol, you must specify the target '
            'api level in the apiLevel key of the metadata. For instance, '
            'metadata={"apiLevel": "2.0"}')
