"""
Copyright 2018  - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocols/__init__.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/protocols/__init__.py
@package pyotronext/protocols
@authors 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)@copyright 2018 - 2022 The Pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
License: Apache License, Version 2.0 (https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pryotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot running Marlin 2.0 firmware.
Inspired by the Opentrons software framework by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
Language: Python 3.7
Status: Alpha
Credits: 
DerAndere: Founder, initial design and implementation, 
           core developer, current maintainer
Other pyotronext authors, see pyotronext AUTHORS file 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS): 
For individual contributions, see CHANGELOG file 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/CHANGELOG)
and the history of the git repository 
(https://gitlab.com/RobotsByDerAndere/pyotronext/commits/master)
Jack Minardi: Development of mecode
Opentrons Labworks Inc.: parts of execute.py, entrypoint_utils.py, 
                         protocols/parse.py, protocols/types.py; labware/*, 
                         design of the opentrons APIv2 (https://github.com/Opentrons/opentrons)

pyotronext is based on third-party software. Each third-party 
software is licensed under the terms of its separate license. The License text 
can be found in the directory 3rd_party_licenses/ . The information in this 
comment block pertains to the respective third-party software. 

mecode (https://github.com/jminardi/mecode)
Copyright (c) 2014 Jack Minardi
License: MIT
Contributions: mecode was developed by the Lewis Lab at Harvard 
University.

opentrons (https://github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0 

@version 1.0.7
"""

__author__      = "DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)"
__copyright__   = "Copyright (c) 2018-2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)"
__license__     = "Apache-2.0"
__version__     = "1.0.7"

