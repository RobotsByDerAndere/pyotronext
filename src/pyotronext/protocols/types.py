"""
Copyright 2018  - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocols/types.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""


"""
@file pyotronext/protocols/types.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other Pyortronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018-2020 DerAndere 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS) 
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a Python package to convert 
opentrons API 2 or mecode compatible protocols into G-code (Marlin dialect) and 
communicate with the liquid handling robot.
Inspired by the opentrons API version2 Python package by Opentrons Labworks Inc. 
See https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/README.md . 
This file contains type definitions including the APIVersion and PythonProtocol 
classes of the opentrons API version 2
Language: Python 3.7
"""

"""
This software is based on third-party software. Each third-party software is 
licensed under the terms of its separate license. The License text
can be found in the directory 3rd_party_licenses/ . The information in this
comment block pertains to the respective third-party software.

opentrons (https://github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0
"""


from typing import NamedTuple, Any, Dict, Union, Optional

Metadata = Dict[str, Union[str, int]]

class APIVersion(NamedTuple):
    major: int
    minor: int

    def __str__(self):
        return f'{self.major}.{self.minor}'

class PythonProtocol(NamedTuple):
    text: str
    filename: Optional[str]
    contents: Any  # This is the output of compile() which we can't type
    metadata: Any
    api_level: APIVersion
    # these 'bundled_' attrs should only be included when the protocol is a zip
    bundled_labware: Optional[Dict[str, Dict[str, Any]]]
    bundled_data: Optional[Dict[str, bytes]]
    bundled_python: Optional[Dict[str, str]]
    # this should only be included when the protocol is not a zip
    extra_labware: Optional[Dict[str, Dict[str, Any]]]


