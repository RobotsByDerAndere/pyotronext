"""
Copyright 2018  - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/entrypoint_utils.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/entrypoint_utils.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other Pyortronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The Pyotronext authors 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/entrypoint_utils.py :
Copyright 2015 - 2020 Opentrons Labworks Inc.
License: Apache License, Version 2.0 
@brief Part of pyotronext. 
@details Based on the opentrons API version2 Python package by Opentrons Labworks Inc.  
This file contains the functions datafiles_from_paths() and labware_from_paths()
of the opentrons API version 2.  
Language: Python 3.7
"""


import json
import jsonschema
import pkgutil
import pathlib

def datafiles_from_paths(paths):
    datafiles={}
    for strpath in paths:
        purepath = pathlib.PurePath(strpath)
        if purepath.is_absolute():
            path = pathlib.Path(purepath)
        else:
            path = pathlib.Path.cwd() / purepath
        if path.is_file():
            datafiles[path.name] = path.read_bytes()
        elif path.is_dir():
            for child in path.iterdir():
                if child.is_file():
                    datafiles[child.name] = child.read_bytes()
    return datafiles


def labware_from_paths(paths):
    labware_defs= {}

    for strpath in paths:
        purepath = pathlib.PurePath(strpath)
        if purepath.is_absolute():
            path = pathlib.Path(purepath)
        else:
            path = pathlib.Path.cwd() / purepath
        if not path.is_dir():
            raise RuntimeError(f'{path} is not a directory')
        for child in path.iterdir():
            if child.is_file() and child.suffix.endswith('json'):
                try:
                    defn = verify_definition(child.read_bytes())
                except Exception as e:
                    raise e
                else:
                    uri = uri_from_definition(defn)
                    labware_defs[uri] = defn
    return labware_defs

def verify_definition(contents):
    """ Verify that an input string is a labware definition and return it.
    If the definition is invalid, an exception is raised; otherwise parse the
    json and return the valid definition.
    :raises json.JsonDecodeError: If the definition is not valid json
    :raises jsonschema.ValidationError: If the definition is not valid.
    :returns: The parsed definition
    """
    schema_body = pkgutil.get_data(  # type: ignore
        'opentrons',
        'shared_data/labware/schemas/2.json').decode('utf-8')
    labware_schema_v2 = json.loads(schema_body)

    if isinstance(contents, dict):
        to_return = contents
        jsonschema.validate(to_return, labware_schema_v2)

    else:
        to_return = json.loads(contents)
        jsonschema.validate(to_return, labware_schema_v2)
    return to_return


def uri_from_details(namespace, load_name, version, delimiter='/'):
    """ Build a labware URI from its details.
    A labware URI is a string that uniquely specifies a labware definition.
    :returns str: The URI.
    """
    return f'{namespace}{delimiter}{load_name}{delimiter}{version}'


def uri_from_definition(definition, delimiter='/'):
    """ Build a labware URI from its definition.
    A labware URI is a string that uniquely specifies a labware definition.
    :returns str: The URI.
    """
    return uri_from_details(definition['namespace'],
                            definition['parameters']['loadName'],
                            definition['version'])
    
    
