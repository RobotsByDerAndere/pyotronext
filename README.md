# pyotronext

Python library for lab robot control. pyotronext is an open source Python package for
conversion of [opentrons APIv2](https://docs.opentrons.com/v2/new_protocol_api.html) 
or [mecode](https://github.com/jminardi/mecode) compatible Python protocols into 
a dialect of the ISO 6983-1 and ANSI/EIA RS274-D G-code is compatible with open source firmware for CNC machines. 
pyotronext can control liquid handling robots via USB using the 8-N-1 asynchronous serial communication protocol 
(8 data bits, no parity, 1 stop bit). 

As an example, pyotronext is used as the backend of the Graphical G-code Generator 
and robot control software [GGCGen](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCgen).
Compatible controller firmware:
- [Marlin2ForPipetBot](https://github.com/DerAndere1/Marlin), 
- [Marlin firmware](https://github.com/MarlinFirmware/Marlin),
- [grblHAL](https://github.com/grblHAL/core),
- [Smoothie](https://github.com/Smoothieware/Smoothieware),
- [Duet3D RepRap firmware](https://github.com/Duet3D/RepRapFirmware),
- [Synthetos g2core](https://github.com/synthetos/g2),
- [Repetier Firmware 2.0](https://github.com/repetier/Repetier-Firmware/tree/dev2),
- [LinuxCNC](https://linuxcnc.org/)


# Getting started - Find help

Please follow the instructions for installation. The user manual can be found
online (https://gitlab.com/RobotsByDerAndere/pyotronext/wikis/pyotronext-User-Manual) 
as part of the pyotronext Wiki 
(https://gitlab.com/RobotsByDerAndere/pyotronext/wikis/wiki_pyotronext_home ).


## Installation:

1. Install a Python 3.7 or Python 3.8 interpreter that comes with all distributions of Python. 
For Windows, I recommend CPython that is part of the [reference Python 
distribution by the Python Foundation](https://www.python.org/downloads/) 
(an executable installer is provided for download). Run the installer by double-
clicking the executable file you downloaded. Follow the instruction on the 
screen and when the installer wizard dialog gives the option to "Install Python launcher for Windows"
and "Install Python in Path", select both options. 

2. Installation of external Python packages except for pyserial is usually done 
automatically during installation of pyotronext with pip. 
If you want to install the dependencies seperately, you can use the tool pip by 
the Python Packaging Authority (PyPa) which is included in the Python 3.7 or Python 3.8
distribution of the Python Foundation (if you don't have pip installed, follow 
[the instructions](https://pip.pypa.io/en/stable/installing/) ). 
Then navigate to the Powershell_ISE.exe and right-click the executable 
Powershell_ISE.exe. In the context  menu that appears, left-click "Run as Administrator". 
To install the development version of pyotronext, type the following in the command prompt and confirm by pressing the 
enter-key:
```
py -3.8 -m pip -r install https://gitlab.com/RobotsByDerAndere/pyotronext/-/archive/master/pyotronext-master.tar.gz
```

Alternatively, install the latest tagged release of pyotronext [pyotronext](https://gitlab.com/RobotsByDerAndere/pyotronext/-/releases).
as follows (replace the `<tag>` by the desired version number (tag)):
```
py -3.8 -m pip install https://gitlab.com/RobotsByDerAndere/pyotronext/-/archive/<tag>/pyotronext-<tag>.tar.gz
```

Or download the distribution package attached to the release notes (preferably 
the binary wheel) and install from the downloaded archive or wheel:
```
py -3.8 -m pip install "<Path/to/downloaded/folder>"
```

### Configuration of pyotronext

Navigate to the /Lib/site-packages/pyotronext inside your Python program folder. 
Open the following files with a program that can edit text files (*txt) and edit 
the parameters to fit your hardware:

- Robot specifications, general configuration: .../pyotronext/src/pyotronext/config.py
- Specifications of labware: .../pyotronext/src/pyotronext/labware


## Using pyotronext

After pyotronext and its dependencies are installed and configured as described in 
the section "Installation" above, navigate to the pyotronext (opentrons APIv2) 
Python script you want to run. Right-click the 
file and in the context menu that appears, left-click 
"Open with..." -> "Select app". Browse your computer and navigate to 
the Python interpreter. For Windows, I recommend CPython which should be in the 
directory .../Python3x/python.exe after installing Python 3x (see above). 
Select the option to use python.exe as the standard 
application to open Python scripts (files with file extension *.py). The protocol 
can be converted to a dialect of the ISO 6983-1 and ANSI/EIA RS274-D G-code that is compatible with LinuxCNC and Marlin firmware. 
pyotronext can control liquid handling robots 
via USB using the 8-N-1 asynchronous serial communication protocol (8 data bits, no parity, 1 stop bit). 
Compatible firmware:
- [Marlin2ForPipetBot](https://github.com/DerAndere1/Marlin)
- [Marlin firmware](https://github.com/MarlinFirmware/Marlin)
- [grblHAL](https://github.com/grblHAL/core)
- [Smoothie](https://github.com/Smoothieware/Smoothieware)

Example script:

Create a text file with extension .py (e.g. protocol1.py) and the following content. 
The file protocol1.py should have the following content:

```python
import pyotronext.execute
from pyotronext import protocol_api 

def run(protocol: protocol_api.ProtocolContext):

    tiprack1 = protocol.load_labware(load_name="gep_96_tiprack_10ul", location=1)
    plate1 = protocol.load_labware(load_name="corning_96_wellplate_360ul_flat", location=1)

    pipette = protocol.load_instrument(instrument_name="p300_single", mount="left", tip_racks=[tiprack1])
    
    pipette.move_to(plate1["A1"])
    
    pipette.teardown()  # finish moves and disconnect
protocol = pyotronext.execute.get_protocol_api(version="2.0") 

run(protocol)
```

`pyotronext.execute.get_protocol_api()` creates a virtual representation
of the lab working space to be controlled. The arguments of this function 
change the behaviour:

@param `version`: `str`, api version. Default: None  

After saving, you can execute the protocol by double-clicking on the file or by running powershell.exe and 
entering the following command: 
`py -3.8 "Drive:Path/To/ProtocolDirectory/protocol1.py"`

To run an existing opentrons compatible Python/pyotronext protocol that was saved
as a Python script (e.g. protocolName.py) from a Python shell, run the following
script:

```python
import pathlib
import pyotronext.execute

protocolDirectory = pathlib.Path("Path/To/ProtocolDirectory")
protocolName = "protocolName"
protocolFileExtension = ".py"
protocolFileName = protocolName + protocolFileExtension
protocolFilePath = protocolDirectory / protocolFileName

with protcolFilePath.open() as protocolFilehandler:
    pyotronext.execute.execute(protocol_file=protocolFilehandler, protocol_name=protocolName)
```

To run an existing opentrons compatible Python/pyotronext protocol that was saved
as a Python script (e.g. protocolName.py) from the command line interpreter 
(terminal or powershell.exe) enter the following in the terminal / PowerShell:
```
py -3.8 -m pyotronext.execute "Path/To/ProtocolDirectory/protocolName.py"
```

To use the equivalent entry point `pyotronext_execute`, enter the following in the terminal / Powershell:
```
pyotronext_execute "Path/To/ProtocolDirectory/protocolName.py"
```

# Developing pyotronext

Get the source code for [pyotronext](https://gitlab.com/RobotsByDerAndere/pyotronext)
by forking pyotronext and cloning the fork using git. If you want to download only
the current files of the selected branch, click the control panel "Download" 
(next to the control panel "clone"). 


## Code structure

The source code is structured as in the table below. Files that need to be 
edited by users for configuration of their robot are _emphasized_, files that
are important for developers are shown with **strong emphasis**: 

| subfolder     | file              | class                 | description            |
|:--------------|:------------------|:----------------------|:-----------------------|
|src/pyotronext/|_config.ini_       |NA                     |Robot dimensions in configObj compatible .INI format. Users should adjust values to match the physical robot to be controlled|
|.../           |configurator.py    |Config                 |Config.config is a dict from the config.ini file.|
|.../           |entrypoint_utils.py|functions: datafiles_from_paths(), labware_from_paths()|Return dict from JSON files|
|.../           |_execute.py_       |functions: get_protocol_api()|Entrypoints of the user-facing API|
|.../           |**protocol_api.py**|ProtocolContext        |ProtocolContext.load_instrument() instantiates vitrualLab.VirtualLab. load_labware() instantiates plate.LabwareItem as class variable |
|.../           |serialCommunication|Communication          |Handles USB serial connection with the the robot hardware
|.../           |**virtualLab.py**  |VirtualLab             |Virtual representation of the lab, provides opentrons API v2 compatible liquid handling commands|
|.../           |labwareItems.py    |LabwareItems           |General representation of labware dimensions as Python object|
|.../protocols/ |parse.py           |functions: parse()     |Protocol file handler|
|.../protocols/ |types.py           |APIversion, Metadata, PythonProtocol|custom types for protocols.parse|
|.../3rd_party_licenses/|LICENSE_[LicenseName]|NA           |original third-party licenses for software components included in pyotronext|
|.../labware/   |_[load_name].JSON_ |NA                     |Labware definitions     |
|               |AUTHORS            |NA                     |List of authors for copyright purposes|
|               |CONTRIBUTING       |NA                     |Contribution guidelines for the GGCGen project|
|               |LICENSE            |NA                     |License with copyright information|
|               |NOTICE             |NA                     |NOTICE for compliance with 3rd-party components licensed under the Apache License, Version 2.0|
|               |README.md          |NA                     |Information about pyotronext|


## Distribute pyotronext

1. Update all version numbers and push the changes
1. Create a tag for the last commit (e.g. tag: 1.0.1, message: "tagged release of pyotronext v1.0.1" and push the changes
1. Download https://gitlab.com/RobotsByDerAndere/pyotronext/-/archive/master/pyotronext-master.tar.gz and extract the archive.
1. run powershell.exe and enter the following command to change directory to the parent directory of the setup.py file within pyotronext:
```
cd "Path\to\parentdirectory\pyotronext
```
To create a wheel (.whl file) ind the dis subdirectory, enter 
```
py -3.8 setup.py bdist_wheel
```
1. log in to gitlab.com
1. go to https://gitlab.com/RobotsByDerAndere/pyotronext/-/releases
1. Left-click "New Release". Release name: v1.0.1, release notes: tagged release of pyotronext 1.0.1 and attach the wheel (*.whl) from above
1. Left-click "OK"


## Credits

Thanks to:

- DerAndere: Initial design and implementation, core developer, current maintainer. ([Donate](https://www.paypal.com/donate/?hosted_button_id=TNGG65GVA9UHE))
- Jack minardi: Development of original [mecode](https://github.com/jminardi/mecode).
- Opentrons Labworks Inc.: parts of execute.py, entrypoint_utils.py, 
   protocols/parse.py, protocols/types.py; labware/*, design of the 
   [opentrons APIv2](https://github.com/Opentrons/opentrons)
- Other contributors: See [file ./AUTHORS](https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS). 
- For individual contributions: See [file ./CHANGELOG](https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/CHANGELOG) 
   and the history of the git repository (https://gitlab.com/RobotsByDerAndere/pyotronext/commits/master)


# License information

```python
"""
SPDX-License-Identifier: Apache-2.0

pyotronext (https://gitlab.com/RobotsByDerAndere/pyotronext) 

Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
See pyotronext AUTHORS file (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)

This work is

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

This project uses the [REUSE framework](https://reuse.software/) to simplify license compliance. For details, see the ./.reuse/*.dep5 file and the [./LICENSES folder](https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/LICENSES) in the root directory of this work. 

This software includes software developed by DerAndere:

Contributions by DerAndere
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)


This software includes software developed by The Pyotronext authors:

Contributions by The Pyotronext authors
Copyright 2018 - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)


This software contains software derived from portions of the Opentrons 
Platform 3.15.2 (parts of opentrons/api/*) by Opentrons Labworks Inc., including 
various modifications by DerAndere 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS).

Opentrons Platform 3.15.2
Copyright 2015 - 2020 Opentrons Labworks, Inc.


This software contains components derived from portions of mecode 0.4.0 by Jack Minardi 
(https://github.com/jminardi/mecode), with various 
modifications by DerAndere 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS).

mecode 0.4.0 (https://github.com/jminardi/mecode)
Copyright 2014 Jack Minardi
This software was developed by the Lewis Lab at Harvard University.
Licensed under the MIT License, with
YEAR: 2014
COPYRIGHT HOLDER: Jack Minardi
"""
```
