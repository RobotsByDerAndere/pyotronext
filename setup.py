"""
Copyright 2018 - 2022 The Pyotronext authors (https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
SPDX-License-Identifier: Apache-2.0
"""

"""
@file pyotronext/setup.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues) and other Pyotronext authors 
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
@copyright 2018 - 2022 The Pyotronext authors
(https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/pyotronext/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of pyotronext. 
@details pyotronext is a robot control software. 
This file contains the setup script for the Python package setuptools.  
Language: setuptools setup.py
"""


import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup()
